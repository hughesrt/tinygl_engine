#ifndef _GL_UTILS_H_
#define _GL_UTILS_H_

#include <GL/glew.h>		// include GLEW and new version of GL on Windows
#include <GLFW/glfw3.h>     // GLFW helper library
#include <stdarg.h>			// used by log functions to have variable number of args
#include "cg_math.h"

/*------------------------------GLOBAL VARIABLES------------------------------*/
extern int g_gl_width;
extern int g_gl_height;
extern GLFWwindow *g_window;

/*--------------------------------LOG FUNCTIONS-------------------------------*/
bool restart_gl_log();
bool gl_log( const char *message, ... );
/* same as gl_log except also prints to stderr */
bool gl_log_err( const char *message, ... );

/*--------------------------------GLFW3 and GLEW------------------------------*/
bool start_gl();
void glfw_error_callback( int error, const char *description );
void glfw_framebuffer_size_callback( GLFWwindow *window, int width, int height );
void _update_fps_counter( GLFWwindow *window );

/*-----------------------------------SHADERS----------------------------------*/
bool parse_file_into_str( const char *file_name, char *shader_str, int max_len );
void print_shader_info_log( GLuint shader_index );
bool create_shader( const char *file_name, GLuint *shader, GLenum type );
bool is_program_valid( GLuint sp );
bool create_program( GLuint vert, GLuint frag, GLuint *program );

/* just use this func to create most shaders; give it vertex and frag files */
GLuint create_program_from_files( const char *vert_file_name, const char *frag_file_name );

bool load_texture( const char *file_name, GLuint *tex );

/*-------------------------------COMPUTE SHADERS------------------------------*/
/*this is just kept seperate for debugging ease, could easily be rolled up*/
bool create_compute_program(GLuint comp, GLuint *compute_program);

GLuint create_compute_program_from_file(const char *comp_file_name);

/*-----------------------------------CAMERA-----------------------------------*/
struct Camera {
	vec3 cam_pos;

	float cam_heading;			// y-rotation in degrees
	float cam_speed;			// 1 unit per second
	float cam_heading_speed;	// 30 degrees per second

	float near;
	float far;
	float fovy;					// in degrees
	float aspect;

	// make a quaternion representing negated initial camera orientation
	float quaternion[4];

	mat4 view_mat;
	mat4 proj_mat;
	mat4 R;
};

void set_view_proj(Camera* camera);

#endif