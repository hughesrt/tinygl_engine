#version 410 core

layout (location = 0) in vec3 pos; // positional data

uniform mat4 V, P;

void main() {
	gl_Position = P * V * vec4 (pos, 1.0);
	gl_PointSize = 5.0; // size in pixels
}