#ifndef _INPUT_H_
#define _INPUT_H_

#include "gl_utils.h"
#include "cg_math.h"
#include <GLFW/glfw3.h>
#define _USE_MATH_DEFINES
#include <math.h>

static bool poll_input(GLFWwindow *g_window, double elapsed_seconds, Camera* camera)
{
	// keep track of some useful vectors that can be used for keyboard movement
	vec4 fwd = camera->R * vec4(0.0, 0.0, -1.0, 0.0);
	vec4 rgt = camera->R * vec4(1.0, 0.0, 0.0, 0.0);
	vec4 up = camera->R * vec4(0.0, 1.0, 0.0, 0.0);

    // control keys
    bool cam_moved = false;
    vec3 move( 0.0, 0.0, 0.0 );
    float cam_yaw = 0.0f; // y-rotation in degrees
    float cam_pitch = 0.0f;
    float cam_roll = 0.0;
    if ( glfwGetKey( g_window, GLFW_KEY_A ) ) {
        move.v[0] -= camera->cam_speed * elapsed_seconds;
        cam_moved = true;
    }
    if ( glfwGetKey( g_window, GLFW_KEY_D ) ) {
        move.v[0] += camera->cam_speed * elapsed_seconds;
        cam_moved = true;
    }
    if ( glfwGetKey( g_window, GLFW_KEY_Q ) ) {
        move.v[1] += camera->cam_speed * elapsed_seconds;
        cam_moved = true;
    }
    if ( glfwGetKey( g_window, GLFW_KEY_E ) ) {
        move.v[1] -= camera->cam_speed * elapsed_seconds;
        cam_moved = true;
    }
    if ( glfwGetKey( g_window, GLFW_KEY_W ) ) {
        move.v[2] -= camera->cam_speed * elapsed_seconds;
        cam_moved = true;
    }
    if ( glfwGetKey( g_window, GLFW_KEY_S ) ) {
        move.v[2] += camera->cam_speed * elapsed_seconds;
        cam_moved = true;
    }
    if ( glfwGetKey( g_window, GLFW_KEY_LEFT ) ) {
        cam_yaw += camera->cam_heading_speed * elapsed_seconds;
        cam_moved = true;

        // create a quaternion representing change in heading (the yaw)
        float q_yaw[4];
        create_versor( q_yaw, cam_yaw, up.v[0], up.v[1], up.v[2] );
        // add yaw rotation to the camera's current orientation
        mult_quat_quat(camera->quaternion, q_yaw, camera->quaternion );

        // recalc axes to suit new orientation
        quat_to_mat4(camera->R.m, camera->quaternion );
        fwd = camera->R * vec4( 0.0, 0.0, -1.0, 0.0 );
        rgt = camera->R * vec4( 1.0, 0.0, 0.0, 0.0 );
        up = camera->R * vec4( 0.0, 1.0, 0.0, 0.0 );
    }
    if ( glfwGetKey( g_window, GLFW_KEY_RIGHT ) ) {
        cam_yaw -= camera->cam_heading_speed * elapsed_seconds;
        cam_moved = true;
        float q_yaw[4];
        create_versor( q_yaw, cam_yaw, up.v[0], up.v[1], up.v[2] );
        mult_quat_quat(camera->quaternion, q_yaw, camera->quaternion );

        // recalc axes to suit new orientation
        quat_to_mat4(camera->R.m, camera->quaternion );
        fwd = camera->R * vec4( 0.0, 0.0, -1.0, 0.0 );
        rgt = camera->R * vec4( 1.0, 0.0, 0.0, 0.0 );
        up = camera->R * vec4( 0.0, 1.0, 0.0, 0.0 );
    }
    if ( glfwGetKey( g_window, GLFW_KEY_UP ) ) {
        cam_pitch += camera->cam_heading_speed * elapsed_seconds;
        cam_moved = true;
        float q_pitch[4];
        create_versor( q_pitch, cam_pitch, rgt.v[0], rgt.v[1], rgt.v[2] );
        mult_quat_quat(camera->quaternion, q_pitch, camera->quaternion );

        // recalc axes to suit new orientation
        quat_to_mat4(camera->R.m, camera->quaternion );
        fwd = camera->R * vec4( 0.0, 0.0, -1.0, 0.0 );
        rgt = camera->R * vec4( 1.0, 0.0, 0.0, 0.0 );
        up = camera->R * vec4( 0.0, 1.0, 0.0, 0.0 );
    }
    if ( glfwGetKey( g_window, GLFW_KEY_DOWN ) ) {
        cam_pitch -= camera->cam_heading_speed * elapsed_seconds;
        cam_moved = true;
        float q_pitch[4];
        create_versor( q_pitch, cam_pitch, rgt.v[0], rgt.v[1], rgt.v[2] );
        mult_quat_quat(camera->quaternion, q_pitch, camera->quaternion );

        // recalc axes to suit new orientation
        quat_to_mat4(camera->R.m, camera->quaternion );
        fwd = camera->R * vec4( 0.0, 0.0, -1.0, 0.0 );
        rgt = camera->R * vec4( 1.0, 0.0, 0.0, 0.0 );
        up = camera->R * vec4( 0.0, 1.0, 0.0, 0.0 );
    }
    if ( glfwGetKey( g_window, GLFW_KEY_Z ) ) {
        cam_roll -= camera->cam_heading_speed * elapsed_seconds;
        cam_moved = true;
        float q_roll[4];
        create_versor( q_roll, cam_roll, fwd.v[0], fwd.v[1], fwd.v[2] );
        mult_quat_quat(camera->quaternion, q_roll, camera->quaternion );

        // recalc axes to suit new orientation
        quat_to_mat4(camera->R.m, camera->quaternion );
        fwd = camera->R * vec4( 0.0, 0.0, -1.0, 0.0 );
        rgt = camera->R * vec4( 1.0, 0.0, 0.0, 0.0 );
        up = camera->R * vec4( 0.0, 1.0, 0.0, 0.0 );
    }
    if ( glfwGetKey( g_window, GLFW_KEY_C ) ) {
        cam_roll += camera->cam_heading_speed * elapsed_seconds;
        cam_moved = true;
        float q_roll[4];
        create_versor( q_roll, cam_roll, fwd.v[0], fwd.v[1], fwd.v[2] );
        mult_quat_quat(camera->quaternion, q_roll, camera->quaternion );

        // recalc axes to suit new orientation
        quat_to_mat4(camera->R.m, camera->quaternion );
        fwd = camera->R * vec4( 0.0, 0.0, -1.0, 0.0 );
        rgt = camera->R * vec4( 1.0, 0.0, 0.0, 0.0 );
        up = camera->R * vec4( 0.0, 1.0, 0.0, 0.0 );
    }
    // update view matrix
    if ( cam_moved ) {
        quat_to_mat4(camera->R.m, camera->quaternion );

        // checking for fp errors
        //	printf ("dot fwd . up %f\n", dot (fwd, up));
        //	printf ("dot rgt . up %f\n", dot (rgt, up));
        //	printf ("dot fwd . rgt\n %f", dot (fwd, rgt));

		camera->cam_pos = camera->cam_pos + vec3( fwd ) * -move.v[2];
		camera->cam_pos = camera->cam_pos + vec3( up ) * move.v[1];
		camera->cam_pos = camera->cam_pos + vec3( rgt ) * move.v[0];
        mat4 T = translate( identity_mat4(), vec3(camera->cam_pos ) );

		camera->view_mat = inverse(camera->R ) * inverse( T );
		return true;
    }
	return false;
}

#endif