#include "gl_utils.h"
#include "cg_math.h"
#include "input.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define _USE_MATH_DEFINES
#include <math.h>
#define GL_LOG_FILE "gl.log"

Camera camera;

int g_gl_width = 640;
int g_gl_height = 480;
GLFWwindow *g_window = NULL;

#define PARTICLE_COUNT 80000

/* create initial attribute values for particles. return a VAO */
GLuint gen_particles() {
	float rand_dist[PARTICLE_COUNT*3];
	create_rand_distrubution(rand_dist, PARTICLE_COUNT*3);
	GLuint velocity_vbo;

	glGenBuffers( 1, &velocity_vbo );
	glBindBuffer( GL_ARRAY_BUFFER, velocity_vbo );
	glBufferData( GL_ARRAY_BUFFER, sizeof(rand_dist), rand_dist, GL_STATIC_DRAW );

	GLuint vao;
	glGenVertexArrays( 1, &vao );
	glBindVertexArray( vao );
	glBindBuffer( GL_ARRAY_BUFFER, velocity_vbo );
	glVertexAttribPointer( 0, 3, GL_FLOAT, GL_FALSE, 0, NULL );
	glEnableVertexAttribArray( 0 );

	return vao;
}

int main() {
	restart_gl_log();
	// use GLFW and GLEW to start GL context. see gl_utils.cpp for details
	start_gl();

	/* create buffer of particle initial attributes and a VAO */
	GLuint vao = gen_particles();

	GLuint shader_program =
		create_program_from_files( "../resources/point_vert.glsl", "../resources/point_frag.glsl" );

	camera.cam_pos = vec3(0.0f, 0.0f, 2.0f);

	camera.cam_heading = 0.0f;				// y-rotation in degrees
	camera.cam_speed = 5.0f;					// 1 unit per second
	camera.cam_heading_speed = 100.0f;		// 30 degrees per second

	camera.near = 0.1f;
	camera.far = 100.0f;
	camera.fovy = 67.0f;	// in degrees
	camera.aspect = (float)g_gl_width / (float)g_gl_height; // aspect ratio

	set_view_proj(&camera);

	/* make up a world position for the emitter */
	vec3 emitter_world_pos( 0.0f, 0.0f, 0.0f );

	// locations of view and projection matrices
	int V_loc = glGetUniformLocation( shader_program, "V" );
	assert( V_loc > -1 );
	int P_loc = glGetUniformLocation( shader_program, "P" );
	assert( P_loc > -1 );
	glUseProgram( shader_program );
	glUniformMatrix4fv( V_loc, 1, GL_FALSE, camera.view_mat.m );
	glUniformMatrix4fv( P_loc, 1, GL_FALSE, camera.proj_mat.m );

	// load texture
	GLuint tex;
	if ( !load_texture( "../resources/Droplet.png", &tex ) ) {
		gl_log_err( "ERROR: loading Droplet.png texture\n" );
		return 1;
	}

	glEnable( GL_CULL_FACE );		// cull face
	glCullFace( GL_BACK );			// cull back face
	glFrontFace( GL_CCW );			// GL_CCW for counter clock-wise
	glDepthFunc( GL_LESS );			// depth-testing interprets a smaller value as "closer"
	glEnable( GL_DEPTH_TEST );		// enable depth-testing
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	glClearColor( 0.2, 0.2, 0.2, 1.0 );
	/* MUST use this is in compatibility profile. doesn't exist in core
	glEnable(GL_POINT_SPRITE);
	*/

	while ( !glfwWindowShouldClose( g_window ) ) {
		static double previous_seconds = glfwGetTime();
		double current_seconds = glfwGetTime();
		double elapsed_seconds = current_seconds - previous_seconds;
		previous_seconds = current_seconds;

		_update_fps_counter( g_window );
		// wipe the drawing surface clear
		glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
		glViewport( 0, 0, g_gl_width, g_gl_height );

		/* Render Particles. Enabling point re-sizing in vertex shader */
		glEnable( GL_PROGRAM_POINT_SIZE );
		glPointParameteri( GL_POINT_SPRITE_COORD_ORIGIN, GL_LOWER_LEFT );

		glEnable( GL_BLEND );
		glDepthMask( GL_FALSE );
		glActiveTexture( GL_TEXTURE0 );
		glBindTexture( GL_TEXTURE_2D, tex );
		glUseProgram( shader_program );

		glBindVertexArray( vao );
		// draw points 0-3 from the currently bound VAO with current in-use shader
		glDrawArrays( GL_POINTS, 0, PARTICLE_COUNT );
		glDisable( GL_BLEND );
		glDepthMask( GL_TRUE );
		glDisable( GL_PROGRAM_POINT_SIZE );

		// update other events like input handling
		glfwPollEvents();

		if(poll_input(g_window, elapsed_seconds, &camera)) {
			glUniformMatrix4fv(V_loc, 1, GL_FALSE, camera.view_mat.m);
		}
		if ( GLFW_PRESS == glfwGetKey( g_window, GLFW_KEY_ESCAPE ) ) {
			glfwSetWindowShouldClose( g_window, 1 );
		}
		// put the stuff we've been drawing onto the display
		glfwSwapBuffers( g_window );
	}

	// close GL context and any other GLFW resources
	glfwTerminate();
	return 0;
}